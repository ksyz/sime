# Simple (SNMP) IF-MIB exporter

## Commandline mode

-H,--host IP_ADDR

-c,--community COMMUNITY_STRING

-I,--only-oid NAME ...
	Include ONLY exactly matching OID names

-E,--excl-oid NAME ...
	Exclude exactly matching OID names

-i,--filter-in TAG=/RE/ ...
	Include ONLY interfaces with tags matching RE


-o,--filter-out TAG=/RE/ ...
	Include ONLY interfaces with tags NOT matching RE

```bash
# exclude ifMtu table fields
# require only table entries with exact ifType
# exclude entries with with ifAlias starting with "TEMP"
# include only ifDescr of mathching description

$ sime -H 127.0.0.1 -c vzumvzum \
-E ifMtu \
-i ifType=ethernetCsmacd \
-o 'ifAlias=^TEMP\s' \
-i 'ifDescr=^(.*Ether|.*Gig|BDI|Tunnel).*|^Port-channel|^Port +[0-9]+$' \
```

`-E,-I` may be specified multiple times. Also `-o,-i` may be specified multiple times, but for each field only once.

## HTTP mode

Requires *hypnotoad* (or similar). Metrics are exported via `/snmp` route.

The argument semantics are the same as for command line:
* filter_in/filter_out => -i/-o
* only_oid/excl_oid => -I/-E

Passed by POST or GET method.

## Systemd integration

Basic sime service utilizes systemd service instances. Therefore, service is started as `sime@PORT_NUMBER` by default. But could be adjusted via environment files to any combination of listening addresses and TCP ports. Just provide unique identifier as instance name.

By default, sime comes preconfigured for port tcp/9117:

`sime@9117.service` will use communities from `/etc/sime/communities@9117.json` and read environment variables, in order, from:
* /etc/sysconfig/sime
* /etc/sysconfig/sime@9117


Default configuration for unconfigured instance will be `/etc/sime/comm.json` for communities as defined in default environment `/etc/sysconfig/sime`.

NetDot exporters use systemd timer units:
* `sime-netdot-communities@.timer` - Fires up communities exports for particular instance. Applies environment file the same way as service instance.
* `sime-netdot-sd.timer` - Creates JSON file suitable for prometheus file_sd service discovery. Byt default in `/etc/prometheus/netdot_sd.json`, configurable via `/etc/sysconfig/sime`.

### Communities
```bash
# timer, replaces cron job
systemctl enable sime-netdot-communities@9117.timer
systemctl start sime-netdot-communities@9117.timer
# to run export
systemctl start sime-netdot-communities@9117.service
```

###  Service Discovery
```bash
systemctl enable sime-netdot-sd.timer
systemctl start sime-netdot-sd.timer

systemctl start sime-netdot-sd.service
```

# Exporting data from NetDot

Create `netdot.conf`:

```ini
[netdot]
rest_url = http://www.example.com/netdot
username = apiuser
password = apipassword
```

in `/etc/sime/netdot.conf` or at place of your convinience, but adjust `SIME_NETDOT_CONF` variable in `/etc/sysconfig/sime` or delegated instance environment files.

## Create communities database

Create a cron job, systemd timer or run manualy:

```bash
./netdot_sd -C comm.json
```
# Create service-discovery list for `file_sd_configs`

Create a cron job, systemd timer or run manualy:

```bash
./netdot_sd -D /etc/prometheus/netdot_sd.json -i backbone_fw -i backbone_gw -E 10.66.6.42
```

## Prometheus job configuration

```yaml
  - job_name: 'simple_ifmib_exporter'
    scrape_interval: 30s
    scrape_timeout: 15s
    file_sd_configs:
      - files:
        - /etc/prometheus/netdot_sd.json
    static_configs:
      - targets:
        - 127.0.0.1
        - 127.0.0.42
    metrics_path: /snmp
    params:
      community: [netcom]
      filter_out: ["ifAlias=^TEMP\s"]
      filter_in: ["ifType=ethernetCsmacd", "ifDescr=^(.*Ether|.*Gig|BDI|Tunnel).*|^Port-channel|^Port +[0-9]+$"]
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: snmpexporter.example.com:9117  # SNMP exporter. 
```

## Using SNMP communities lookup file

```bash
$ export SIME_COMMUNITIES_DB=/etc/sime/comm.json
$ netdot_sd -C "$SIME_COMMUNITIES_DB"
$ SIME_LISTEN='http://*:9117' hypnotoad -f ./sime
```


