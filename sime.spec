%global commit0 ###REV###
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

%define _username snmp_exporter

Name:		sime
Version:	1
Release:	5.git_%{shortcommit0}%{?dist}
Summary:	Simple IF-MIB Exporter for Prometheus

License:	WTFPL
URL:		https://git.in.o2bs.sk/mingeli/sime
Source0:	https://git.in.o2bs.sk/mingeli/%{name}/repository/archive.tar.gz?ref=%{commit0}#/%{name}-%{shortcommit0}.tar.gz
# Source1:	sime@.service
# Source2:	sysconfig.sime

BuildArch:	noarch

%{?systemd_requires}
BuildRequires:	systemd

Requires:		perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Requires(pre):	shadow-utils
# needed for randomized time delays introduced in 229
# Requires:		systemd >= 229
Requires:		systemd
Requires:		perl(Mojolicious)

%{?perl_default_filter}
# Everything is merged into single file, so no provided and required packages.
%global __requires_exclude perl\\(VMS|perl\\(Win32|perl\\(module_|perl\\(Sime::
%global __provides_exclude perl\\(VMS|perl\\(Win32|perl\\(DB\\)|perl\\(UNIVERSAL\\)|perl\\(module_|perl\\(Sime::

%description
Simple IF-MIB exporter, for Prometheus and a-likes. Strips down IF-MIB 
tables to some useful datasets, without the hassle of pregenerated 
snmp_exporter configurations.

%prep
%setup -q -n %{name}-%{commit0}-%{commit0}

%build
exit 0

%install
%define _conf_dir %{buildroot}%{_sysconfdir}/%{name}

install -d -m 750 %{_conf_dir}
install -d -m 755 %{buildroot}%{_sysconfdir}/sysconfig/
install -m 755 sysconfig.sime %{buildroot}%{_sysconfdir}/sysconfig/%{name}
install -d -m 755 %{buildroot}%{_bindir}
install -d -m 755 %{buildroot}%{_libexecdir}/%{name}
install -d -m 750 %{buildroot}/%{_sharedstatedir}/%{name}

install -m 755 "sime" %{buildroot}%{_bindir}
install -m 755 "netdot_sd" %{buildroot}%{_libexecdir}/%{name}

# systemd services
install -d -m 755 %{buildroot}%{_unitdir}
install -m 644 *.timer *.service %{buildroot}%{_unitdir}

# https://fedoraproject.org/wiki/Packaging:Scriptlets
%post
exit 0

%postun
exit 0

# https://fedoraproject.org/wiki/Packaging:UsersAndGroups
%pre
getent passwd %{_username} >/dev/null || \
    useradd -r -g nobody -d %{_sharedstatedir}/%{name} -s %{_bindir}/nologin \
    -c "SNMP Exporter" %{_username}
exit 0

%files
%doc README.md 
%config %attr(-,%{_username},nobody) %{_sysconfdir}/%{name}
%config %{_sysconfdir}/sysconfig/*
%{_bindir}/*
%{_unitdir}/*
%{_libexecdir}/%{name}/*
%{_sharedstatedir}/%{name}

%changelog
* Thu Oct 26 2017 Michal Ingeli <mi@v3.sk> 1-3
- Fixed pidfile location

* Thu Oct 26 2017 Michal Ingeli <mi@v3.sk> 1-2
- Fixed release of netdot_sd
- Fixed path to sysconfig

* Wed Oct 25 2017 Michal Ingeli <mi@v3.sk> 1-1
- Initial packaging
