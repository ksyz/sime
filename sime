#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use Getopt::Long qw(:config no_ignore_case bundling);
use SNMP;
use Socket; # inet_ntoa
use Mojolicious::Lite;
use Mojo::JSON qw(decode_json encode_json);
use Net::IP;
use File::stat;
use File::Slurp "read_file";
use Scalar::Util qw(blessed);

my (
	$comm, $dest, $help, $cdb, $timeout,
	%filter_in, %filter_out,
	%only_oids, %excl_oids
);

$help = <<HELP;
-H,--host IP_ADDR

-c,--community COMMUNITY_STRING

-I,--only-oid NAME ...
	Include ONLY exactly matching OID names

-E,--excl-oid NAME ...
	Exclude exactly matching OID names

-i,--filter-in TAG=/RE/ ...
	Include ONLY interfaces with tags matching RE


-o,--filter-out TAG=/RE/ ...
	Include ONLY interfaces with tags NOT matching RE

-C,--communities DBFILE
	JSON with address => community map

-t,--timeout SECS
	SNMP timeout, two retries only.
HELP

my $options = GetOptions(
	'h|help!' => sub { print $help; exit 0 },

	'H|host=s' => \$dest,
	'c|community=s' => \$comm,

	'I|only-oid=s@' => sub { make_filter(\%only_oids, $_[1]) },
	'E|excl-oid=s@' => sub { make_filter(\%excl_oids, $_[1]) },

	'i|filter-in=s@' => sub { make_filter(\%filter_in, $_[1]) },
	'o|filter-out=s@' => sub { make_filter(\%filter_out, $_[1]) },
	't|timeout=i' => sub { $timeout = $_[1] * 1000*1000 }, # SNMP{Timeout} uses micro-seconds
);

sub make_filter {
	my ($target, $value) = @_;
	my ($k, $v) = split('=', $value, 2);
	$v ||= 1;
	$target->{$k} = $v;
}

helper read_communities => sub {
	my $c = shift;

	my $ctime = stat($c->app->config('communities_db'))->mtime;
	my $prev_ctime = $c->app->defaults('communities_changed');
	if (!$c->app->defaults('communities') || !defined $prev_ctime || $prev_ctime < $ctime ) {
		my $cdb = $c->app->config('communities_db');
		$c->app->log->debug(sprintf("Loading communities DB from %s", $cdb));
		my $cdb_data = read_file($cdb);
		my $communities = decode_json($cdb_data);
		$c->app->defaults('communities', $communities);
		$c->app->defaults('communities_changed', $ctime);
	}

	if (@_) {
		$c->app->log->debug(sprintf("Searching community DB for %s", $_[0]));
		my $communities = $c->app->defaults('communities');
		if (defined $communities->{$_[0]}) {
			my $community = $communities->{$_[0]};
			$c->app->log->debug(sprintf("Community DB for %s: %s", $_[0], $community));
			return $community;
		}
		else {
			$c->app->log->error(sprintf("Community DB for %s is not defined", $_[0]));
			return undef;
		}
	}
	else {
		return $c->app->defaults('communities');
	}
};

&SNMP::initMib();

my %oid_device_metadata = ( 
	sysLocation => "System location", 
	sysUpTime => "System up time", 
	hrSystemUptime => 'System Up Time', 
	# hrMemorySize => "Installed memory size",
	sysName => "System name",
	laLoad => "System load",

	ssIOSent => "IO Sent",
	ssIOReceive => "IO Receive",
	ssSysInterrupts => "system Interrupts",
	ssSysContext => "system context switches",
	# deprecated
	ssCpuUser => "CPU User",
	ssCpuSystem => "CPU System",
	ssCpuIdle => "CPU Idle",
	
	ssCpuRawUser => "The number of 'ticks' (typically 1/100s) spent processing user-level code.",
	ssCpuRawNice => "The number of 'ticks' (typically 1/100s) spent processing reduced-priority code.",
	ssCpuRawSystem => "The number of 'ticks' (typically 1/100s) spent processing system-level code.",
	ssCpuRawIdle => "CPU Raw idle",
	ssCpuRawWait => "CPU Raw wait",
	ssCpuRawKernel => "CPU Raw kernel",
	ssCpuRawInterrupt => "CPU Raw Interrupts",
	ssCpuRawSoftIRQ => "CPU raw soft IRQ",

	ssIORawSent => "IO Raw Sent",
	ssIORawReceived => "IO Raw Received",
	
	ssRawInterrupts => "Raw interrupts",
	ssRawContexts => "Raw contexts",
	# ssRawSwapIn.0 = Counter32: 0
	# ssRawSwapOut.0 = Counter32: 0


	# memIndex.0 = INTEGER: 0
	# memErrorName.0 = STRING: swap
	# memTotalSwap.0 = INTEGER: 0 kB
	# memAvailSwap.0 = INTEGER: 0 kB
	memTotalReal => "Memory total",
	memAvailReal => "Memory available",
	memTotalFree => "Memory free",
	# memMinimumSwap.0 = INTEGER: 16000 kB
	memShared => "Memory shared",
	memBuffer => "Memory buffered",
	memCached => "Memory cached",
	# memSwapError.0 = INTEGER: error(1)
	# memSwapErrorMsg.0 = STRING: Running out of swap space (0)

);
my %oid_device_text = ( sysDescr => "System version and description" );
my @oid_device_data = (keys %oid_device_metadata, keys %oid_device_text);

my %oid_state = ( 
	ifAdminStatus => "The desired state of the interface", 
	ifOperStatus => "The current operational state of the interface" 
);
my %oid_tags = ( 
	ifAlias => "Provides a non-volatile handle for the interface, specified by a network manager.",
	ifName => "Provides a non-volatile handle for the interface, specified by a network manager.",
	ifDescr => "Textual string containing information about the interface",
	ifIndex => "if[X]Table interface index",
	ifType => "IANA specified media type"
);
my %oid_settings = ( 
	ifMtu => "The size of the largest packet which can be sent/received on the interface, specified in octets",
	ifSpeed => "An estimate of the interface's current bandwidth in bits per second"
);
my %oid_counters = (
	(map { $_ => "The number of packets, delivered by this sub-layer to a higher (sub-)layer" } qw( ifHCInBroadcastPkts ifHCInMulticastPkts ifHCInOctets ifHCInUcastPkts )),
	(map { $_ => "The number of packets that higher-level protocols requested be transmitted, and which were addressed to a address at this sub-layer, including those that were discarded or not sent" } qw( ifHCOutBroadcastPkts ifHCOutMulticastPkts ifHCOutOctets ifHCOutUcastPkts)),
);
my @mibs = (keys %oid_state, keys %oid_settings, keys %oid_counters, keys %oid_tags, @oid_device_data);

# my @mibs = (@oid_state);
# empty list, used for some autoamticaly generated mib names
my %oid_extra = ();

# Mojo::Lite handler
# http://nmp2.in.o2bs.sk:9117/snmp?target=185.103.145.26&community=netcom&filter_in=ifType%3DethernetCsmacd&filter_out=ifAlias%3D%5EPRIPRAVA
any "/snmp" => sub {
	my $c = shift;

	my $target = $c->param('target');
	eval {
		$target = Net::IP->new($target) or die (Net::IP::Error());
	};
	if ($@) {
		return $c->render(text => 'Invalid target', format => 'txt')
	}
	
	my $community = $c->param('community');

	unless ($community) {
		$community = $c->app->read_communities($target->ip);
	}

	return $c->render(text => 'Missing target IP and community', format => 'txt')
		unless $target && $community;

	my $filters = {};
	for my $i (qw(filter_in filter_out excl_oids only_oids)) {
		$filters->{$i} = {};
		my $args = $c->every_param($i);
		for (@$args) {
			make_filter($filters->{$i}, $_);
		}
	};

	my $data;

	eval {
		$data = Sime::gather($target->ip, $community, $filters);
	};
	if ($@) {
		if ( blessed $@ && $@->isa('Sime::TimeOutError') ) {
			$c->app->log->error($@);
			return $c->render(text => '### '.$@, status => 504, format => 'txt');
		}
		else {
			die $@;
		}
	}

	return $c->render(text => $data, format => 'txt');
};

if (exists $ENV{MORBO_VERBOSE} || defined $ENV{MOJO_APP_LOADER} && $ENV{MOJO_APP_LOADER}) {
	# app->config(communities_db => 'comm.json');
	my %config = (
		communities_db => $ENV{SIME_COMMUNITIES_DB},
		hypnotoad => {
			pid_file => ($ENV{SIME_PID_FILE} ? $ENV{SIME_PID_FILE} : undef), 
		},
	);

	# timeout is global.
	$timeout = $ENV{SIME_SNMP_TIMEOUT} * 1000 * 1000
		if defined $ENV{SIME_SNMP_TIMEOUT};

	if (defined $ENV{SIME_LISTEN_PORT}) {
		$config{hypnotoad}{listen} = [
			sprintf("%s://%s:%s", 
				($ENV{SIME_LISTEN_HTTPS} ? 'https' : 'http'),
				($ENV{SIME_LISTEN_ADDR} ? $ENV{SIME_LISTEN_ADDR} : '*'),
				($ENV{SIME_LISTEN_PORT}))
		];
	}
	else {
		$config{hypnotoad}{listen} = [$ENV{SIME_LISTEN},];
	}

	app->config(%config);
	app->start;
}
elsif ($options && $comm && $dest) {
	print Sime::output($dest, $comm);
	1;
}
else {
	print STDERR <<HELP;
Run either in command line mode or HTTP server mode.

* hypnotoad HTTP server

  SIME_COMMUNITIES_DB=comm.json SIME_LISTEN=http://*:666 hypnotoad -f sime

* Command line mode

  $0 -H 1.2.3.4 -c faminly_community

HELP
	exit 1;
}

package Sime;

use strict;
use warnings;

use Data::Dumper;
use SNMP;
use Socket; # inet_ntoa
use Net::IP;
use Scalar::Util "looks_like_number";

sub gather {
	my ($dest, $comm, $filters) = @_;

	# Initialize the MIB (else you can't do queries).
	my $sess = SNMP::Session->new(
		Community => $comm,
		DestHost => inet_ntoa(inet_aton($dest)),
		Version => '2c',
		UseSprintValue => 0,
		($timeout ? (Timeout => $timeout) : ()),
		Retries => 2,
	);

	# Used to hold the individual responses.
	my $var;  

	# Turn the MIB object into something we can actually use.
	# $vb = new SNMP::Varbind([$mib,'0']); # '0' is the instance.

	my %interfaces = ();
	my %device_data = ();

	if ($filters->{only_oids} && ref $filters->{only_oids} && scalar keys %{$filters->{only_oids}}) {
		@mibs = (@oid_device_data, keys %oid_tags, grep { exists $filters->{only_oids}{$_} } @mibs);
	}

	if ($filters->{excl_oids} && ref $filters->{excl_oids} && scalar keys %{$filters->{excl_oids}}) {
		@mibs = (@oid_device_data, keys %oid_tags, grep { ! exists $filters->{excl_oids}{$_} } @mibs);
	}

	my $vars = SNMP::VarList->new(map { [ $_ ] } @mibs);

	my @resp = $sess->bulkwalk(0, 48, $vars);

	die Sime::TimeOutError->new("Cannot do bulkwalk on <$dest> with community <$comm>: $sess->{ErrorStr} ($sess->{ErrorNum})", snmp => $sess)
		if $sess->{ErrorNum};

	# Print out the returned response for each variable.
	my $i = 0;
	for my $vbarr ( @resp ) {
		next
			if (!defined $vbarr || 1 > scalar @$vbarr);
		# Determine which OID this request queried.  This is kept in the VarList
		# reference passed to bulkwalk().
		# my $oid = $vars->[$i++]->tag();
		my $oid = $vbarr->[0]->tag;
		
		# Count the number of responses to this query.  The count will be 1 for
		# non-repeaters, 1 or more for repeaters.
		# my $num = scalar @$vbarr;
		# print "$num responses for oid $oid";
		

		# Display the returned list of varbinds using the SNMP::Varbind methods.
		if ($oid =~ /^if/) {
			my $last_index = -1;
			for my $v (@$vbarr) {
				die Sime::SnmpError->new("Error: OID not increasing. Offending index: $oid.".$v->iid)
					if $v->iid <= $last_index;

				$last_index = $v->iid;

				$interfaces{$v->iid} = {}
					unless exists $interfaces{$v->iid};

				if ($v->val =~ /^up|down$/) {
					$interfaces{$v->iid}->{$oid} = $v->val eq 'up' ? 1 : 0;
				}
				elsif ($oid =~ /Octets$/) {
					$interfaces{$v->iid}->{$oid} = $v->val;
					
					my $bits_oid = $oid =~ s/Octets$/Bits/gr;
					$oid_extra{$bits_oid} = 1; # extra generated $oid
					$interfaces{$v->iid}->{$bits_oid} = $v->val * 8;
				}
				else {
					$interfaces{$v->iid}->{$oid} = $v->val;
				}

				# printf("\t%s = %s (%s)\n", $v->name, $v->val, $v->iid);
			}
		}
		elsif ($oid =~ /^(sys|la|hr|ss|mem)/) {
			for my $v (@$vbarr) {
				if ($oid =~ /^hrSystemUptime/) {
					my $val = $v->val;
					my ($d, $h, $m, $s) = split (/:/, $val);
					# $device_data{$oid} = $s+($m*60)+($h*3600)+($d*86400);
					$device_data{$oid} = $val;
					$device_data{sysUpTime} = $device_data{$oid};
				}
				elsif ($oid =~ /^laLoad/) {
					print Dumper($v);
					if ($v->iid == 1) {
						$device_data{node_load1} = $v->val;
						$device_data{laLoad1} = $v->val;
					}
					elsif ($v->iid == 2) {
						$device_data{node_load5} = $v->val;
						$device_data{laLoad5} = $v->val;
					}
					elsif ($v->iid == 3) {
						$device_data{node_load15} = $v->val;
						$device_data{laLoad15} = $v->val;
					}
				}
				else {
					$device_data{$oid} = $v->val;
				}
			}
		}
	}

	my $gauges;
	my $buf = '';
	INT: for my $i (keys %interfaces) {
		for my $m (keys %oid_tags) {
			next INT
				if ( defined $filters->{filter_out}{$m} 
				&& $interfaces{$i}->{$m} =~ /$filters->{filter_out}{$m}/);
			next INT
				if ( defined $filters->{filter_in}{$m} 
				&& $interfaces{$i}->{$m} !~ /$filters->{filter_in}{$m}/);
		}

		for my $m (keys %oid_counters, keys %oid_state, keys %oid_settings, keys %oid_extra) {
			# we do some overrides
			next 
				unless defined $interfaces{$i}->{$m};

			$gauges->{$m} = []
				unless defined $gauges->{$m};

			push @{$gauges->{$m}}, { 
				value => $interfaces{$i}->{$m},
				labels => {map { $_ => $interfaces{$i}->{$_} } keys %oid_tags }
			};
		}
	};

	for my $g (keys %$gauges) {
		$buf .= gauge($g, 'some help', 'gauge', $gauges->{$g});
	}

	for my $t (keys %oid_device_metadata, keys %device_data) {
		next 
			unless defined $device_data{$t};

		if (looks_like_number($device_data{$t})) {
			$buf = gauge($t, '', 'untyped', [{ 
				value => $device_data{$t}, 
				labels => {}
				}]).$buf;
		}
		else {
			$buf = gauge($t, '', 'untyped', [{ 
				value => 1, 
				labels => {
					$t => $device_data{$t}
				}}]).$buf;
		}
	}

	for my $t (keys %oid_device_text) {
		next unless defined $device_data{$t};
		
		my $text = sprintf("### %s <%s>\n", $t, $device_data{$t});
		$text =~ s/\r\n/ /mg;
		$buf = $text.$buf;
	}

	return $buf;
};

sub gauge {
	my ($name, $help, $type, $values) = @_;
	my $buf = '';

	$buf .= sprintf("# HELP %s %s\n", $name, $help)
		if $help;
	$buf .= sprintf("# TYPE %s %s\n", $name, $type)
		if $type;

	for my $gauge (@$values) {
		my $label = join(',', 
			map { sprintf('%s="%s"', $_, $gauge->{labels}{$_}) }
			grep { defined $gauge->{labels}{$_} && $gauge->{labels}{$_} ne '' }
			sort keys %{$gauge->{labels}});

		$buf .= sprintf("%s{%s} %s\n", $name, $label, $gauge->{value});
	}

	return $buf;
};

sub output {
	my ($target, $community) = @_;

	eval {
		$target = Net::IP->new($target) or die (Net::IP::Error());
	};
	if ($@) {
		die Sime::InvalidAddressError->new('Invalid target');
	}

	die Sime::ArgumentError->new('Missing target IP and community')
		unless $target && $community;

	return gather($target->ip, $community, {
		filter_in => \%filter_in,
		filter_out => \%filter_out,

		excl_oids => \%excl_oids,
		only_oids => \%only_oids,
	});
};

package Sime::Error;
use base qw(Error);
use overload ('""' => 'stringify');

sub new {
	my $self = shift;
	my $text = "" . shift;
	my @args = (@_);

	my $class = ref($self) || $self;

	local $Error::Depth = $Error::Depth + 1;
	local $Error::Debug = 1;  # Enables storing of stacktrace

	return $self->SUPER::new(-text => $text, @args);
}

package Sime::InvalidAddressError;
use base 'Sime::Error';

package Sime::ArgumentError;
use base 'Sime::Error';

package Sime::SnmpError;
use base 'Sime::Error';

package Sime::TimeOutError;
use base 'Sime::Error';
